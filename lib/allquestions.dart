import 'question.dart';

class AllQuestions {
  int _number = 0;

  List<Question> _questions = [
    Question('You can lead a cow down stairs but not up stairs.', false),
    Question('Approximately one quarter of human bones are in the feet.', true),
    Question('A slug\'s blood is green.', true),
  ];

  void nextQuestion() {
    if (_number < _questions.length - 1) {
      _number++;
    }
    print('$_number  --  ${_questions.length}');
  }

  bool isFinished() {
    if (_number >= _questions.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  String getQuestionIndex() {
    return _questions[_number].question;
  }

  bool getAnswerIndex() {
    return _questions[_number].answer;
  }

  void reset() {
    _number = 0;
  }
}
